import {describe, it} from "mocha";
import {assert, expect} from 'chai';
import {parseHourToMinute} from "@/time";

describe("parseHourToMinute()", () => {
    it("should round to minutes", () => {
        assert.equal(parseHourToMinute(1.505), 90);
    });

    it("should get minutes (1.5 hour)", () => {
        assert.equal(parseHourToMinute(1.5), 90);
    });

    it("should get minutes 0", () => {
        assert.equal(parseHourToMinute(0), 0);
    });

    it("should throw for string", () => {
        // when
        expect(() => parseHourToMinute('14')).to.throw('Expected float time');
    });

    it("should throw for null", () => {
        // when
        expect(() => parseHourToMinute(null)).to.throw('Expected float time');
    });
});
