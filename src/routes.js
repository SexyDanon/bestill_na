import {FormLayout, Location, Details, Availability, Payment} from '@/pages/Form/';

const routes = [
    {
        path: "/",
        component: FormLayout,
        redirect: "/",
        children: [
            {
                path: "/",
                name: "Location",
                component: Location,
            },
            {
                path: "/details",
                name: "Details",
                component: Details,
            },
            {
                path: "/availability",
                name: "Availability",
                component: Availability,
            },
            {
                path: "/payment",
                name: "Payment",
                component: Payment,
            },
            {
                path: "*",
                redirect: "/"
            }
        ]
    }
];

export default routes;
