export default class Counter {
    constructor(min, max, valueCallback) {
        this.min = min;
        this.max = max;
        this.value = 0;
        this.interval = null;
        this.valueCallback = valueCallback;
    }

    startCounting(inc) {
        clearInterval(this.interval);
        this.interval = setInterval(() => {
            this.value += inc;
            this.valueCallback(inc);
        }, 200);
    }

    stopCounting() {
        clearInterval(this.interval);
    }
}
