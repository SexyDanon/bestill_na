import i18n from './i18n';
import Translate from './Translate';

export {i18n, Translate};
