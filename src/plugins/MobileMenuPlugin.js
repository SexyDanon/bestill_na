const SidebarStore = {
    showSidebar: false,
    displaySidebar(value) {
        this.showSidebar = value;
    }
};

const SidebarPlugin = {
    install(Vue) {
        Vue.mixin({
            data() {
                return {
                    sidebarStore: SidebarStore
                };
            }
        });

        Object.defineProperty(Vue.prototype, "$sidebar", {
            get() {
                return this.$root.sidebarStore;
            }
        });
    }
};

export default SidebarPlugin;
