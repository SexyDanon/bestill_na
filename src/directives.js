function onlyNumbers(el, binding, vnode) {
    el.addEventListener('paste', event => {
        const data = (event.clipboardData || window.clipboardData).getData('Text');

        if (/^\d+$/.test(data)) {
            return true;
        }
        event.preventDefault();
        return false;
    });

    el.addEventListener('keypress', event => {
        let charCode = event.which || event.keyCode;
        if (charCode < 32 || (charCode >= 48 && charCode <= 57)) {
            return true;
        }
        event.preventDefault();
        return false;
    });
}

export {onlyNumbers};
