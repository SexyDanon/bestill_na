import {parsePhoneNumberFromString} from 'libphonenumber-js';

export const sum = (partial_sum, a) => partial_sum + a;

export const unicodeLetters = text => /\w{2,}/u.test(text);

export const emailValidator = email => /[^\s@]+@[^\s@]+\.[^\s@]+/.test(email);

export const isValidPhoneNumber = (phoneNumber, countryCode) => {
    const result = parsePhoneNumberFromString(phoneNumber, countryCode);
    if (typeof result === "undefined") {
        return undefined;
    }
    return result.isValid();
};

export const formatPhoneNumber = (phoneNumber, countryCode) => {
    const result = parsePhoneNumberFromString(phoneNumber, countryCode);
    if (typeof result === "undefined") {
        throw `Invalid phone number: ${phoneNumber}:${countryCode}`
    }
    return result.number;
};

export const objectToFormData = (object) => {
    return objectToFormDataRec(object, new FormData())

    function objectToFormDataRec(obj, form, namespace) {
        for (let property in obj) {
            if (obj.hasOwnProperty(property)) {
                let formKey;
                if (namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }

                if (typeof obj[property] === 'object') {
                    objectToFormDataRec(obj[property], form, property);
                } else if (typeof obj[property] === 'boolean') {
                    form.append(formKey, obj[property] ? '1' : '0');
                } else {
                    form.append(formKey, obj[property]);
                }
            }
        }

        return form;
    }
};
