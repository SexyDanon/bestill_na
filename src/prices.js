export function getPricePerHour(breakpoints, area) {
    const {pricePerHour, time} = breakpointByArea(breakpoints, area);
    return {
        price: pricePerHour * time / 60,
        time
    };
}

export function getPriceAbsolute(breakpoints, area) {
    const {priceAbsolute, time} = breakpointByArea(breakpoints, area);
    return {
        price: priceAbsolute,
        time
    };
}

function breakpointByArea(breakpoints, area) {
    if (!Array.isArray(breakpoints)) {
        throw "Invalid breakpoints";
    }
    if (breakpoints.length === 0) {
        throw "Empty breakpoints";
    }
    let sort = breakpoints
        .filter(breakpoint => breakpoint.area > area)
        .sort((one, two) => one.area - two.area);

    if (sort.length === 0) {
        throw "High price";
    }

    return sort[0];
}
